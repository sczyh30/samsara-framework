##SEP 提案
Samsara Framework Enhancement-Proposal  
Started at 2015-7-4  
Updated at 2015-7-11

---

###SEP 01:让Samsara Framework支持函数式编程特性，引入@FunctionalInterface annotation
Introduction:Add functional programming features for Samsara  
Owner:sczyh30  
Since(JDK Version):JDK 1.8  
Since(Framework Version):0.1  
Created:2015-7-7  
Updated:No  
Status:Going  
Issue  

###SEP 02: 使用Gradle构建Samsara Framework项目
Introduction:Add functional programming features for Samsara  
Owner:sczyh30  
Since(JDK Version):JDK 1.8  
Since(Framework Version):0.1  
Created:2015-7-11  
Updated:No  
Status:Going  
Issue  

###SEP 03:使Samsara Merak MVC支持解析Markdown模板
Introduction:Add markdown template support
 for Samsara Merak MVC view resolver engine  
Owner:sczyh30  
Since(JDK Version):JDK 1.8  
Since(Framework Version):0.1  
Created:2015-7-11  
Updated:No  
Status:Going  
Issue  

